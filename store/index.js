export const state = () => ({
  access_token: localStorage.getItem('access_token') || null,
  refresh_token: localStorage.getItem('refresh_token') || null,
  isLoggedIn: false
})

export const getters = {
  getAccessToken(state) {
    return state.access_token
  },

  getRefreshToken(state) {
    return state.refresh_token
  },

  getIsLoggedIn(state) {
    return state.isLoggedIn
  }
}

export const mutations = {
  loginUser(state) {
    state.isLoggedIn = true
  },

  logoutUser(state) {
    state.isLoggedIn = false
  },
}



