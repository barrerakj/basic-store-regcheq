export default function ({ redirect, store, route }){
  if(route.path !== '/login' && store.state.isLoggedIn === false)
    return redirect('/login')
}
