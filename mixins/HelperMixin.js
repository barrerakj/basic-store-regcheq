const HelperMixin = {
  methods: {
    formTitle( entityName, slug ) {
      return slug === 'new' ? 'New ' + entityName : entityName + ' Details'
    },
  },
}

export default HelperMixin;
